# Projeto 2 - 2020/2 - Central

## Compilação

Para compilar, digite o comando abaixo dentro do diretório do projeto:

`$ make`

## Execução

Para executar o programa, digite o comando abaixo:

`$ make run` 

## Utilização

Ao executar o programa, a tela será atualizada a cada **1 segundo** com os estados dos dispositivos e sensores, assim como a temperatura e umidade do ambiente (se o distribuído estiver rodando).

A GUI possui uma tabela enumerada para orientar os comandos: ligar (L) e desligar (D).

![gui](https://i.imgur.com/YGKTJ5U.png)

O uso é dado pela a ação (L ou D) e o dispositivo alvo (0~6). Por exemplo, para ligar a Lampada da Cozinha (1), envie:

`L 1`

Cada dispositivo e sensor possui um número entre colchetes à direita, esse número indica o **estado** atual. **0** representa desligado e **1** ligado.

**OBS:** Devido a atualização da tela, a entrada da ação (L ou D) pode ser removida da tela caso demore mais de 1 segundo para enviar um comando, então é necessário um pouco de cuidado para não enviar um comando como: L L 1.

