#include "server.h"

void init_logger() {
    logger = fopen("logger.csv", "w+");
    fprintf(logger, "Data/Hora, Evento\n");
}

void add_log(char* msg) {
	time_t rawtime;
	struct tm *info;
	char datetime[80];
	time(&rawtime);
	info = localtime(&rawtime);
	strftime(datetime, 80, "%d/%m/%Y %X", info);   

    fprintf(logger, "%s, %s\n", datetime, msg);
}

void message_handler(int type, float obj) {
    if (type == SENSOR) {
        disp[(int)obj+6] = !disp[(int)obj+6];
		if (alrm == 1) {
			add_log("Alarme acionado!");
		}
	}
    else if (type == TEMPERATURE) {
        temperature = obj;
	}
    else {
        humidity = obj;
	}
}

void send_message(int pin, int state) {
    char message[3];
	char buffer[30];

    connect_to_distributed();
    disp[pin-1] = state;

	if (state == 0) {
		sprintf(buffer, "Dispositivo %d desligado", pin);
		add_log(buffer);
	}
	else {
		sprintf(buffer, "Dispositivo %d ligado", pin);
		add_log(buffer);
	}

    message[0] = pin + '0';
    message[1] = state + '0';
    message[2] = '\0';

    if(send(clientSocket, message, 3, 0) != 3)
        printf("Erro no envio: numero de bytes enviados diferente do Recebidos = 0");

    close(clientSocket);
}

void client_handler(int clientSocket) {
	char buffer[6];
	int rxLength;
    int type;
    float obj;

	if((rxLength = recv(clientSocket, buffer, 6, 0)) < 0)
		printf("Erro no recv()\n");

    type = buffer[0] - '0';
    memcpy(&obj, &buffer[1], 4);
    message_handler(type, obj);

	while (rxLength > 0) {
		if(send(clientSocket, buffer, rxLength, 0) != rxLength)
			printf("Erro no envio - send()\n");
		
        if((rxLength = recv(clientSocket, buffer, 6, 0)) < 0)
			printf("Erro no recv()\n");
	}

}

void start_listening() {
    unsigned int clientLength;
	while(1) {
		clientLength = sizeof(clientAddr);
		if((clientSocket = accept(centralServerSocket, 
			                      (struct sockaddr *) &clientAddr, 
			                      &clientLength)) < 0)
			printf("Falha no Accept\n");
		
		//printf("Conexão do Cliente %s\n", inet_ntoa(clientAddr.sin_addr));
		
		client_handler(clientSocket);
		close(clientSocket);
	}
}

void start_server() { 
    alrm = 0;
    for (int i = 0; i < 14; i++) {
        disp[i] = 0;
    }
    setup_server();
    start_listening();
}

void connect_to_distributed() {
	if((clientSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("Falha no socker do Servidor\n");

    memset(&distServerAddr, 0, sizeof(distServerAddr));
	distServerAddr.sin_family = AF_INET;
	distServerAddr.sin_addr.s_addr = inet_addr("192.168.0.4");
	distServerAddr.sin_port = htons(10132);
	if(connect(clientSocket, (struct sockaddr *) &distServerAddr, 
								sizeof(distServerAddr)))
		printf("Erro no connect\n");
}

void setup_server() {
	if((centralServerSocket = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		printf("Falha no socker do Servidor\n");

	memset(&centralServerAddr, 0, sizeof(centralServerAddr));
	centralServerAddr.sin_family = AF_INET;
	centralServerAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	centralServerAddr.sin_port = htons(10032);

	if(bind(centralServerSocket, (struct sockaddr *) &centralServerAddr, sizeof(centralServerAddr)) < 0)
		printf("Falha no Bind\n");

	if(listen(centralServerSocket, 10) < 0)
		printf("Falha no Listen\n");		
}

void shut_down_server() {
    close(clientSocket);
    close(centralServerSocket);
}
