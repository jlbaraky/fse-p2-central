#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include "server.h"
#define clear() printf("\033[H\033[J")

void shut_down() {
    shut_down_server();
    exit(0);
}

void gui() {
    alarm(1);
    clear();
    printf("0. Alarme [%d]\n", alrm);
    printf("1. Lampada Cozinha [%d]\n", disp[0]);
    printf("2. Lampada Sala [%d]\n", disp[1]);
    printf("3. Lampada Quarto 01 [%d]\n", disp[2]);
    printf("4. Lampada Quarto 02 [%d]\n", disp[3]);
    printf("5. Ar-Condicionado Quarto 01 [%d]\n", disp[4]);
    printf("6. Ar-Condicionado Quarto 02 [%d]\n\n", disp[5]);
    printf("Sensores: 0 [%d] - 1 [%d] - 2 [%d] - 3 [%d] - 4 [%d] - 5 [%d] - 6 [%d] - 7[%d]\n",
        disp[6], disp[7], disp[8], disp[9], disp[10], disp[11], disp[12], disp[13]);
    printf("Temperatura: %.2f - Umidade: %.2f\n\n", temperature, humidity);
    printf("(L) Ligar\n");
    printf("(D) Desligar\n");
}

int main() {
    pthread_t id;
    char input;
    int opt;

    signal(SIGINT, shut_down);
    signal(SIGALRM, gui);
    init_logger();
    pthread_create(&id, NULL, (void*)start_server, NULL);

    gui();

    while(1) {
        scanf("%c", &input);
        switch (input) {
            case 'L':
                scanf(" %d", &opt);
                if (opt != 0) 
                    send_message(opt, 1);
                else
                    alrm = 1;
                break;

            case 'D':
                scanf(" %d", &opt);
                if (opt != 0) 
                    send_message(opt, 0);
                else
                    alrm = 0;
            break;

        }
    }

    return 0;
}
