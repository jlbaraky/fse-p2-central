#ifndef SERVER_H
#define SERVER_H

#include <stdio.h>
#include <time.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h> 

#define SENSOR 0
#define TEMPERATURE 1
#define HUMIDITY 2

FILE *logger;
float temperature, humidity;
int disp[14];

// Sockets
int centralServerSocket;
int clientSocket;
// Addresses
struct sockaddr_in centralServerAddr;
struct sockaddr_in distServerAddr;
struct sockaddr_in clientAddr;


void message_handler(int, float);
void send_message(int, int);
void client_handler(int);
void start_listening();
void start_server();
void connect_to_distributed();
void setup_server();
void shut_down_server();
void init_logger();
void add_log(char*);

int alrm;

#endif
